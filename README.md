# README #

This repository contains software and data for use in the GRADnet course on Observational Astrophysics.

### Contents ###

admin - Programme, list of attendees + other admin stuff

DataMining - scripts and data files for the Data Mining session on Monday afternoon.

TelescopeProposal - scripts for the telescope proposal session

DataAnalysis - scripts for the data analysis session

Lectures - keynote lectures

### Individual package notes ###

Currently none

### Contacts ###

* Peter Thomas: p.a.thomas@sussex.ac.uk
