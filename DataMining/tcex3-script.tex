\documentclass[a4paper,11pt]{article}

\input mylatex
\input units
\parindent=0pt
\pagestyle{empty}

\begin{document}

\titleline{Data-mining exercise 3: TAP services, SQL, and jupyter notebooks.}
\bigskip

The aim of this exercise is to introduce you to working with SQL queries to TAP
database servers, and to the use of jupyter notebook for running python scripts
to read in and analyse data.  We will do this using simulated galaxy data from
the Millennium Simulation suite, but the same techniques are applicable also to
observational databases.

Without creating an account, we only have access to the small
``milliMillennium'' simulation data which is in a box of side 62.5\,$h^{-1}$Mpc.
That gives dark matter halo catalogues and also the results at different
snapshots (redshifts) of a few different semi-analytic models of galaxy
formation.  However, it is straightforward to request an account to gain access
to the full data in a box of side 500\,$h^{-1}$Mpc with many more SAMs and also
light-cone data.

Direct access to the Millennium database held at Garching can be obtained via
this link:\\
{\tt http://gavo.mpa-garching.mpg.de/MyMillennium/}\\
There you will find a full list of other databases/tables.  By clicking on the
name of the table on the list on the left-hand-side you will be directed to help
pages describing the data sets.

\section{Accessing data}

There are various ways of accessing and manipulating data.  We will look at three:
\bsi
\item TopCat
\item Python / jupyter notebook
\item SciServer / CasJobs
\esi

\subsection{TopCat}
Instructions for accessing simulation data via TopCat can be found at\\
 {\tt
  http://www.star.bris.ac.uk/~mbt/topcat/sun253/GavoTableLoadDialog.html}
\bsi
\item Start up TopCat.
\esi
\parbox[b]{9cm}{
\bsi
\item Click in the main menu bar on the Load Table icon:
\item Click on the simulation query icon:
\esi 
}\hspace*{0.5cm}
\resizebox{!}{20pt}{ \includegraphics{tcex3-script_fig2.png}
  \includegraphics{tcex3-script_fig3.png}
}
\bsi
\item This should pop up a window with a default query, but if not then:
\bssi
\item Set the Base URL to  {\tt http://gavo.mpa-garching.mpg.de/Millennium};
\item Set the SQL query to
\begin{verbatim}
select top 1000 * 
from millimil..Guo2010a
where snapnum=63
\end{verbatim}
\essi
This will download all properties for the first 1000 entries from the table
Guo2010a in the database millimil that have the property snapnum equal to 63 --
more on SQL syntax below.
\item Play around with the catalogue to see what properties there are.  Example
  plots that you might want to produce are:
\bssi
\item A 3-D $x$, $y$, $z$ plot showing the galaxy locations in the box.  You
  should find that this subset of the data is clustered into particular regions.
\item The main sequence -- star formation rate versus stellar mass.
\item Metallicity versus stellar mass.
\item A colour magnitude diagram of your choice.
\item A colour vs age diagram.
\item etc.
\essi
Note that, because we have downloaded a subset of all galaxies, you can't use
this data-set to produce mass or luminosity functions (but see exercises below).
\esi

\subsection{Python / jupyter notebook}
I was going to show you how to use {\tt pyodbc} (python Online DataBase
Connection) to run remote SQL queries and import the results directly into
python.  Unfortunately I can't work out how to access the Millennium server -- I
am hoping that Mark can help me!

As an alternative, you can use TopCat and then save the data to disk in a variety
of possible formats -- that's much easier for a one-off data extraction.  I have
done that for the query below and saved the data as milliMil.fits. 

It's also possible to use the {\tt wget} command to save the output directly to
a file.  Instead of extracting all the properties for the first 1000 galaxies,
the following selects some of the properties for all galaxies with a stellar
mass exceeding 0.1 (in units of 10$^{10}h^{-1}$M$_\odot$):
\begin{verbatim}
wget \
  --cookies=on --keep-session-cookies \
  --save-cookies=cookie.txt  --load-cookies=cookie.txt \
  -O milli.dat \
  "http://gavo.mpa-garching.mpg.de/MyMillennium?action=doQuery&SQL= \
  select type, centralMvir, stellarMass, bulgeMass, coldGas, hotGas, \
  metalsStellarMass, metalsBulgeMass, metalsColdGas, metalsHotGas, \
  blackHoleMass, sfr, massWeightedAge, xrayLum, \
  u_mag, g_mag, r_mag, i_mag, z_mag, uDust, gDust, rDust, iDust, zDust \
  from millimil..Guo2010a \
  where snapnum=63 and stellarMass>0.1"
\end{verbatim}
The output file can then be read into python, but will need some work in order
to extract all the column names and formats.  It's also possible to call {\tt
  wget} directly from python if you want to embed that command in a script.

\subsection{SciServer (CasJobs)}
Sciserver is a relatively new way of gaining access to the CasJobs server to
many observational databases, including SDSS, Galex and others.  The major
advantage it has over TopCat is that the data can be stored and manipulated
remotely (using {\tt jupyter notebook}s) rather than having to download it to
your local machine.

You will need to register for an account at {\tt
  http://portal.sciserver.org/login-portal/} but then you will be given your own
disk space and the possibility to create different `container's which each
contain a separate python environment (or {\tt R} or {\tt matlab} \ldots).
It is possible to create groups of  people working on the same project who can
share directories (and, I think, containers).

Within SciServer there is a CasJobs module that handles interactions with the
CasJobs data server.   The data is extracted by {\tt SciServer.CasJobs.executeQuery()}
using SQL queries, similar to that shown above.

\section{Using SQL}
SQL or ``Structured Query Language'' is a way of selecting data from databases
with commands that are written in a semi-readable way.  It is very useful for
simply queries (but can be incredibly frustrating for complex ones).  

There is an excellent tutorial for SQL here: {\tt https://www.w3schools.com/sql/}
However, the best way to learn is through examples.  Try the following out on the
millimil..Guo2010a database within TopCat.

Note: SQL is extremely picky about the ordering of commands, so don't rearrange
the lines.  It doesn't care about spacing or capitalisation - you can mess
around with that as much as you like to make your queries look pretty.  However,
it seems to occasionally require you to put in commas in random places, so watch
out for that.

\subsection{A basic query}
\begin{verbatim}
select top 1000 * 
from millimil..Guo2010a
where snapnum=63
\end{verbatim}
\bsi
\item {\tt SELECT  *} -- select all columns from the table.
\item {\tt TOP  1000} -- return only the first 1000 entries (useful when
  testing queries).
\item {\tt FROM  millimil..Guo2010a} -- from the named database and table.
\item {\tt WHERE  snapnum=63} -- for the rows where a particular condition is
  satisfied.  Here snapnum is a column name.
\esi

\subsection{Using multiple selection criteria}
\begin{verbatim}
select *
  from millimil..DeLucia2006a
 where snapnum=63
   and mag_b between -26 and -18 
   and x between 10 and 20
   and y between 10 and 20
   and z between 10 and 20
\end{verbatim}
\bsi
\item {\tt AND } -- used to combine selection conditions.
\item {\tt BETWEEN  x  AND  y} -- used to select a range.
\esi
In this case we select all galaxies with co-ordinates between 10 and 20 in each
axis, and in the absolute magnitude range $-26$ to $-18$ in the b-band.

\subsection{Combining two different views of the same table}
\begin{verbatim}
select PROG.*
  from millimil..DeLucia2006a PROG,
       millimil..DeLucia2006a DES
 where DES.galaxyId = 0
   and PROG.galaxyId between DES.galaxyId and DES.lastprogenitorId
\end{verbatim}
This example finds all the progenitors of the galaxy with {\tt galaxyId}$=0$ at all
earlier times.  The tree is depth-first ordered so that the required galaxies
all have {\tt galaxyId}s in the indicated range.  The query relies on making
selections from two different views of the table and then imposing a condition
that relates the two.

The thing to note is that you can give the views of the tables different names
and then use these throughout the rest of the query (even in lines that precede
that naming).

\subsection{Returning selected columns}
Here is a more complicated example that returns selected columns from three
different views of the same table.  Note the requirement to rename columns that
have ambiguous names.  See if you can work out what the query does.
\begin{verbatim}
select D.galaxyId,       
       D.snapnum,       
       D.g_mag as d_g_mag,       
       D.sfr as d_sfr,    
       P1.g_mag as p1_g_mag,       
       P2.g_mag as p2_g_mag,       
       D.stellarMass as d_mass,       
       P1.stellarMass as p1_mass,       
       P2.stellarMass as p2_mass       
  from millimil..Guo2010a P1,       
       millimil..Guo2010a P2,       
       millimil..Guo2010a D       
 where P1.SNAPNUM=P2.SNAPNUM       
   and P1.galaxyId< P2.galaxyId       
   and P1.descendantId = D.galaxyId       
   and P2.descendantId = D.galaxyId       
   and P1.stellarMass >= .2*D.stellarMass       
   and P2.stellarMass >= .2*D.stellarMass       
   and D.g_mag <-20
\end{verbatim}

\subsection{A more complicated example to return a luminosity function}
\begin{verbatim}
select .2*(.5+floor(r_mag/.2)) as mag, 
       count(*) as num 
  from millimil..Guo2010a 
 where r_mag < -10 
   and snapnum=63 
 group by .2*(.5+floor(r_mag/.2)) 
 order by mag
\end{verbatim}
\bsi
\item Note the use of mathematics to group galaxies into magnitude bins of width
  0.2.
\item {\tt GROUP  BY} and {\tt COUNT } -- rather than returning the
  individual entries, we group them sets (corresponding to our desired magnitude
  bins) and then return a count of the number of entries in each group.  You can
  also return the mean ({\tt AVG }) or sum ({\tt SUM }).
\item {\tt ORDER  BY} -- returns the results in ascending (or descending)
  order by the column(s) listed.
\esi

\subsection{Creating a temporary table for use in a query}
This final example shows how to create a temporary table using an SQL query that
then forms part of an enclosing query.
\begin{verbatim}
select h.haloID,
       avg(h.m_Crit200) as haloMass,
       avg(h.velDisp) as velDisp,
       sum(g.stellarMass) as totGalMass,
       avg(g.stellarMass) as avgGalMass,
       avg(g.u_mag-g.g_mag) as avgGalCol
  from (  select haloID     
            from millimil..Guo2010a         
           where stellarMass > 1  
        group by haloID    
          having count(galaxyID) > 10  ) c,
       millimil..MPAhalo as h,
       millimil..Guo2010a as g
 where h.haloID = c.haloID
   and g.haloID = c.haloID
 group by h.haloID
\end{verbatim}
\bsi
\item The sub-query here finds all halos which have 10 or more galaxies with a mass
greater than 1 (in units of 10$^{10}h^{-1}$M$_\odot$).  That sub-query is written
into a table named 'c'.
\item The table can then be used as usual in the rest of the query.
\item Note that although the mass and velocity dispersion of the halo are the
  same for all entries with the same haloID (because it's the same halo repeated
  over and over for each galaxy that matches), SQL cannot know that and so it
  insists that we use either {\tt COUNT}, {\tt AVG} or {\tt SUM} to reduce the
  output to a single number for each row returned.
\esi

\subsection{Try it yourself}
Work through the above examples, then try some exercises of your own.  Here are
some suggestions:
\bsi
\item Contrast the star formation rates (sfr) of galaxies of different mass
  (stellarMass) and/or environment (centralMvir gives the mass of the halo that
  the galaxy resides in).
\item Contrast the colours of galaxies with different ratios of bulgeMass to stellarMass.
\item Find the mass at $z=0$ (snapnum 63) for the 1000 most massive halos at z=6
  (snapnum 18).
\esi

\section{jupyter notebook}

{\tt jupyter notebook}s are becoming a popular way of mixing text and python
code in scripts (they work for a few other languages too).  They are used by
SciServer to allow you to process data remotely without the need to download it
to your own machine.  They are an excellent way of producing well-documented
scripts.

I have produced a notebook entitled notebook.ipynb with some examples that use
the milliMil data downloaded above.  You can run the examples and follow along,
or look at the PDF printout notebook.pdf

\subsection{Preliminaries}
\begin{verbatim}
# Imports
import numpy as np
from astropy.io import fits           # To read fits files
from astropy.cosmology import WMAP7   # For the WMAP7 cosmology used in Guo2010a
import astropy.constants as c
import astropy.units as u
import matplotlib.pyplot as plt
%matplotlib inline            
import seaborn as sns
\end{verbatim}
\bsi
\item I strongly recommend that, in all but the most basic of scripts, you
  always give your imports names and never use {\tt from <module> import *}.
  The latter leads to name conflicts and will one day cause you immense grief.
\item {\tt numpy} is the basic numerical package in python. 
\item For manipulating tables you may well want to import {\tt pandas}
  but I have steered clear of that in this basic example.
\item {\tt astropy} is an astronomical package that can do lots of useful
  things.  I will use it here to handle units and to calculate cosmological
  parameters from the WMAP7 cosmological model.
\item {\tt matplotlib} is the standard plotting package in python.  The {\tt
  \%matplotlib inline} magic draws plots in-place within the notebook.
\item {\tt seaborn} is a package to produce prettier plots.  One nice feature is
  that you can easily set contexts (paper, notebook, talk, and poster) and
  styles.
\esi
\begin{verbatim}
# Parameters
dataFile='milliMil.fits'
# Simulation parameters (ideally should be read from metadata)
hubble=WMAP7.H(0)/(100*u.km/(u.s*u.Mpc))   # Dimensionless hubble parameter, h
rhoc=WMAP7.critical_density(0)             # Critical density (units included)
mumH=1e-27*u.kg                            # Mean mass per particle in hot gas
boxside=62.5             # Mpc/h
massUnit=1e10            # Msun/h
# Plotting style
sns.set()
sns.set_context('notebook',font_scale=1.5)
sns.set_style('whitegrid')
\end{verbatim}
\bsi
\item I like to define as many parameters as possible at the top of my script.  That
makes it easy to go back in and change things without having to hunt through the
whole script to find stuff.
\item Try to avoid all ``magic numbers'' in the body of code, such as 62.5 for
  the boxside -- define all these up front.
\item Try to make your variable names self-evident.  Comment any that are not.
  Always note what you units are.
\item Note that {\tt astropy} lets you define variables with attached units.  It
  takes a little getting used to but it is much safer than doing things by hand
  (and utlimately easier, more accurate and less error-prone).
\item I am a fan of always including grid lines in my plots.
\esi
\begin{verbatim}
# Read in fits file and examine data
# Open the file
hdulist = fits.open(dataFile)
# Read the data into a numpy record array
data = hdulist[1].data
# Close the file
hdulist.close()
# and list the columns
data.columns
\end{verbatim}
\bsi
\item Reading fits files with the {\tt astropy.io.fits} package is really easy.
\item The only weird thing to note about fits files is that the data that you
  want is stored in the second record (number 1 in python) and not the first
  (number 0).
\item The data is read in as an astropy table that contains metadata, such as
  the column names.
\esi

\subsection{BH-bulge mass relation}
As an example of a scatter plot, we are going to contrast the black hole - bulge
mass relation for central galaxies in a halo, and satellite galaxies.
\begin{verbatim}
# Plotting parameters
xmin=1e9
xmax=1e12
ymin=1e5
ymax=1e11
massRatio=0.003
pltFile='BHBulge.png'
# Extract required data, converting masses to Msun/h
Type=data['type']
blackHoleMass=data['blackHoleMass']*massUnit
bulgeMass=data['bulgeMass']*massUnit
# Plot central galaxies in halos (Type==0) in blue; other galaxies in red
index0=np.where(Type==0)[0]
index12=np.where(np.logical_not(Type==0))[0]
# Create the figure
plt.figure(0,figsize=(8,6))
plt.loglog(bulgeMass[index0],blackHoleMass[index0],'b.',label='Type 0')
plt.loglog(bulgeMass[index12],blackHoleMass[index12],'r.',label='Type 1 & 2')
# Plot linear relation
plt.plot([xmin,xmax],[massRatio*xmin,massRatio*xmax],'k',label=r'$M_\mathrm{BH}/M_\mathrm{Bulge}=$'+str(massRatio))
# Add labels (always!)
plt.legend(loc=2)
plt.xlabel(r'$M_\mathrm{Bulge}/h^{-1}M_\odot$')
plt.ylabel(r'$M_\mathrm{BH}/h^{-1}M_\odot$')
# Set limits of plot
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
# Save results
plt.savefig(pltFile)
\end{verbatim}
\bsi
\item I like to extract the data into {\tt numpy} arrays before manipulating and
  plotting.  That keeps the original data intact.
\item Don't rename your variables unnecessarily -- that just leads to
  confusion.  I had to use a capital {\tt T} in {\tt Type} here because {\tt
    type} is a reserved keyword in python.
\item The {\tt np.where} function extracts indices that match a particular
  condition (it's a bit more sophisticated than that, so check out the
  documentation if you run into difficulties or want to know more).  In this
  case {\tt Type}$=0$ corresponds to central galaxies.
\item Using logical operators ({\tt and, or, not}) with numpy arrays is fraught
  with danger unless you use the numpy functions designed for that purpose.
\item The {\tt '.'} symbol in matplotlib gives a small dot and is very useful if
  you have lots of points.  The {\tt ','} symbol is even smaller -- a single
  pixel.
\item Always, always, always, label your plots and include the units.  Someone
  might look at that plot in 3 years time and want to know what it means, maybe
  when writing their thesis!
\item You can use \LaTeX\ in labels but if you do so you have to precede the
  label string by an 'r' to indicate it is a raw string and prevent the
  $\backslash$ characters being misinterpreted.
\esi

\subsection{Stellar mass function}
The stellar mass function is included as an example of a histogram plot, and
also plotting of error bars on data points.
\begin{verbatim}
# Bins for histogram
binperdex=10
xrange=np.array([9.2,11.6])
nbin=int((xrange[1]-xrange[0])*binperdex+0.5)
# Plotting parameters
xmin=12.4
xmax=7.0
ymin=10**(-5.9)
ymax=10**0.5
pltFile='SMF.png'
# Observational data
obsFile='StellarMassFunction_z0.00.txt'
# Extract required data
stellarMass=data['stellarMass']*massUnit*hubble  # Units Mpc/h**2
# Put into bins and normalise to number per unit volume (Mpc/h) per dex
nobj,bins,junk=plt.hist(np.log10(stellarMass), bins=nbin, range=xrange, log=True)
# Plot at centre of bins
x=0.5*(bins[:-1]+bins[1:])
# Normalise
y=nobj*binperdex/boxside**3
# Plot SMF of data
plt.figure(0,figsize=(8,6))
plt.semilogy(x,y,'-r',label='StellarMass')
plt.axis([xmin,xmax,ymin,ymax])
plt.xlabel(r'$\log_{10}(M_*/h^{-2}M_\odot)$')
plt.ylabel(r'$\log_{10}(N/(\mathrm{dex}\ (h^{-1}\mathrm{Mpc})^3)$')
plt.grid(True)
# Plot Observations
fobs = open(obsFile, 'r')     
nline = int(fobs.readline())
obs=np.empty([nline,4])
for iline in range(nline): obs[iline,:]=fobs.readline().split()
fobs.close()
plt.errorbar((obs[:,0]+obs[:,1])/2.,obs[:,2],xerr=(obs[:,1]-obs[:,0])/2.,yerr=obs[:,3],
                 fmt='o', markersize=5, ecolor='blue', color='blue',label='MCMC fitting data')
plt.legend(loc=2)
plt.savefig(pltFile)
\end{verbatim}
\bsi
\item I have used {\tt plt.hist} to do the binning, but {\tt np.histogram} is a
  good alternative.  There is also the extremely useful {\tt np.digitize}
  function that returns a list of the bin number corresponding to each element
  of the input array.
\esi

\subsection{X-ray luminosity temperature relation}
Unfortunately the Guo2010a model does a terrible job at predicting the X-ray
luminosity of the intra-halo gas, but I wanted to include an example of how to
use {\tt astropy} to do calculations involving physical quantities.
\begin{verbatim}
# Extract required data
Type=data['type']
# Normally would recommend not renaming data fields, but this better describes the data:
xrayLum_log10=data['xrayLum']              # Units erg/s
centralMvir=data['centralMvir']*massUnit   # Msun/h

# Restrict to central halos
index0=np.where(Type==0)[0]
xrayLum_log10=xrayLum_log10[index0]
centralMvir=centralMvir[index0]

# Here is the calculation of the virial temperature
# First convert mass to a quantity with units.  Need double precision to hold high powers
mVir=np.double(centralMvir)*c.M_sun/hubble
rVir=(3*mVir/(800*np.pi*rhoc))**(1/3)
kTvir=mumH*c.G*mVir/(2*rVir)
# Check that have done calculations properly.  The 'to' converts to those units
print('mVir[0]={:.3g}'.format(mVir[0].to(u.M_sun)))
print('rVir[0]={:.3g}'.format(rVir[0].to(u.Mpc)))
print('Tvir[0]={:.3g}'.format((kTvir[0]/c.k_B).si))
# Now make the plot
xmin=0.001
xmax=10.
pltFile='LxTx.png'
# matplotlib does not understand units, so need to make sure that we convert our
# physical quantities to the desired units before exporting:
x=kTvir.to(u.keV)
y=10**xrayLum_log10
plt.figure(0,figsize=(8,6))
plt.loglog(x,y,'.')
plt.xlabel(r'$kT_\mathrm{vir}/$keV')
plt.ylabel(r'$L_\mathrm{X}/$erg$\,$s$^{-1}$')
plt.xlim([xmin,xmax])
plt.plot(np.array([xmin,xmax]),10**44.67*(np.array([xmin,xmax])/5)**3.04,label=r'$10^{44.67}(kT/5$keV$)^{3.04}$')
plt.legend()
plt.savefig(pltFile)
\end{verbatim}
\bsi
\item First I define a version of the virial mass, {\tt mVir}, that has units
  attached.
\item We can then do calculations involving {\tt mVir} and {\tt astropy}
  constants and units.  At each stage, the new variable has units attached.
\item {\tt astropy} variables come as a {\tt value} and a {\tt unit}.  The
  physical quantity stored is the product of these two.  They
  can be extracted by {\tt <variable>.value} and {\tt <variable>.unit}.
\item Variables can be expressed in different units by using the {\tt to()}
  method (or alternatively {\tt .si}): e.g.~in the above {\tt
    x=kTvir.to(u.keV)}.  Note that the quantity stored
  in the variable does not change, it is merely expressed in a different way.
\item Important: when exporting variables to python modules that cannot handle
units, only the value is passed.  It is therefore very important to make sure
that the variable is expressed in the correct units before exporting.  This is a
major gotcha.
\esi

\subsection{Exercises}

Try out some or all of the following exercises.  If you are fully conversant
with {\tt jupyter notebook} then act as a tutor for others, or devise an
exercise of your own.
\bsi
\item Work through the examples shown above to make sure that you understand
  them.
\item Make a plot of the star formation efficiency of halos (the ratio of the
  stellar mass to the halo mass).
\item In the above plot, divide the region into mass bins and show the mean and
  standard deviation of the SFE in each bin (as a point + error bars).
\item In the above plot add a shaded polygon corresponding to the region between
  the 15.9 and 84.1 percentiles {\tt np.percentile}.  You will probably have to
  look for examples of how to construct such a polygon (it requires appending
  two arrays one of which is reversed).
\esi
\end{document}
